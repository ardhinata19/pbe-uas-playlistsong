const routes = (handler) => [
    {
        method: 'POST',
        path: '/playlists/{id}/songs',
        handler: handler.addSongPlaylistHandler,
    },
    {
        method: 'GET',
        path: '/playlists/{id}/songs',
        handler: handler.getAllSongPlaylistsHandler,
    },
];
module.exports = routes;