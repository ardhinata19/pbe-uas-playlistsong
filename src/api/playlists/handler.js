class playlistHandler {
    constructor(service, validator) {
        this._service = service;
        this._validator = validator;

        this.addPlaylistHandler = this.addPlaylistHandler.bind(this);
        this.getAllPlaylistsHandler = this.getAllPlaylistsHandler.bind(this);
        this.getDetailPlaylistHandler = this.getDetailPlaylistHandler.bind(this);
        this.addSongPlaylistHandler = this.addSongPlaylistHandler.bind(this);
        this.getAllSongPlaylistsHandler = this.getAllSongPlaylistsHandler.bind(this);
    }

    async addPlaylistHandler(request, h) {
        try {
            this._validator.validatePlaylistPayload(request.payload);
            const {name} = request.payload;
            const playlistId = await this._service.addPlaylist({name});
            const response = h.response({
                status: 'success',
                message: 'Playlist Berhasil ditambahkan',
                data: {
                    PlaylistId: playlistId
                }
            })
            response.code(201);
            return response;
        } catch (e) {
            const response = h.response({
                status: 'fail',
                message: e.message
            })
            response.code(400);
            return response;
        }
    }

    async getAllPlaylistsHandler() {
        const playlists = await this._service.getPlaylists();
        return {
            status: 'success',
            data: {
                playlists
            }
        };
    }

    async getDetailPlaylistHandler(request, h) {
        try {
            const {id} = request.params;
            const playlist = await this._service.getDetailPlaylist(id);
            return {
                status: 'success',
                data: {
                    playlist
                }
            };
        } catch (error) {
            const response = h.response({
                status: 'fail',
                message: error.message,
            });
            response.code(404);
            return response;
        }
    }

    async addSongPlaylistHandler(request, h) {
        try {
            this._validator.validatePlaylistSongPayload(request.payload);
            const {id} = request.params;
            const playlist_id = id;
            // console.log(playlist_id);
            const {song_id} = request.payload;
            const playlistSongId = await this._service.addSongPlaylist(playlist_id, {song_id});
            const response = h.response({
                status: 'success',
                message: 'Lagu berhasil ditambahkan ke playlist',
                data: {
                    playlistSongId: playlistSongId
                }
            })
            response.code(201);
            return response;
        } catch (e) {
            const response = h.response({
                status: 'fail',
                message: e.message
            })
            response.code(400);
            return response;
        }
    }

    async getAllSongPlaylistsHandler(request, h) {
        try {
            const {id} = request.params;
            // console.log(id);
            const playlistSong = await this._service.getAllSongPlaylist(id);
            return {
                status: 'success',
                data: {
                    playlistSong
                }
            };
        } catch (error) {
            const response = h.response({
                status: 'fail',
                message: error.message,
            });
            response.code(404);
            return response;
        }
    }
}

module.exports = playlistHandler;