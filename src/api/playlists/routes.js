const routes = (handler) => [
    {
        method: 'POST',
        path: '/playlists',
        handler: handler.addPlaylistHandler,
    },
    {
        method: 'GET',
        path: '/playlists',
        handler: handler.getAllPlaylistsHandler,
    },
    {
        method: 'GET',
        path: '/playlists/{id}',
        handler: handler.getDetailPlaylistHandler,
    },
    {
        method: 'POST',
        path: '/playlists/{id}/songs',
        handler: handler.addSongPlaylistHandler,
    },
    {
        method: 'GET',
        path: '/playlists/{id}/songs',
        handler: handler.getAllSongPlaylistsHandler,
    },
];
module.exports = routes;