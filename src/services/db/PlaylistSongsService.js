const {nanoid} = require("nanoid");
const {Pool} = require('pg');

class PlaylistSongsService {
    constructor() {
        this._pool = new Pool();
    }

    async addSongPlaylist(playlist_id,{song_id}) {
        const id = nanoid(16);
        const createAt = new Date().toISOString();
        const query = {
            text: 'INSERT INTO playlist_songs VALUES($1,$2,$3,$4,$5) RETURNING id',
            values: [id, playlist_id, song_id, createAt, createAt]
        }
        // console.log(query);
        const result = await this._pool.query(query);
        if (!result.rows[0].id) {
            throw new Error("Lagu gagal ditambahkan ke playlist");
        }
        return result.rows[0].id;
    }

    async getAllSongPlaylist(id) {
        const query = {
            text: 'SELECT songs.id, songs.title, songs.year, songs.artist, songs.gendre, ' +
                ' songs.duration, playlists.name FROM playlist_songs, playlists, songs ' +
                'WHERE playlist_songs.song_id = songs.id AND playlist_songs.playlist_id = $1 ',
            values: [id]
        }
        // console.log(query);
        const result = await this._pool.query(query);
        console.log(result.rows);
        if(result.rows === []){
            throw new Error('teset');
        }
        if (!result.rows.length) {
            throw new Error('Playlist tidak ditemukan');
        }
        return result.rows[0];
    }
}

module.exports = PlaylistSongsService;