const {nanoid} = require("nanoid");
const {Pool} = require('pg');

class PlaylistsService {
    constructor() {
        this._pool = new Pool();
    }

    async addPlaylist({name}) {
        const id = 'Playlist-' + nanoid(10);
        const createAt = new Date().toISOString();

        const query = {
            text: 'INSERT INTO playlists VALUES($1,$2,$3,$4) RETURNING id',
            values: [id, name, createAt, createAt]
        }

        const result = await this._pool.query(query);
        if (!result.rows[0].id) {
            throw new Error("Playlist gagal ditambahkan");
        }
        return result.rows[0].id;
    }

    async getPlaylists() {
        const result = await this._pool.query('SELECT * FROM playlists');
        return result.rows;
    }

    async getDetailPlaylist(id) {
        const query = {
            text: 'SELECT * FROM playlists where id=$1',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error('Playlist tidak ditemukan');
        }
        return result.rows[0];
    }

    async addSongPlaylist(playlist_id, {song_id}) {
        const id = 'PS-' + nanoid(10);
        const createAt = new Date().toISOString();
        const query = {
            text: 'INSERT INTO playlist_songs VALUES($1,$2,$3,$4,$5) RETURNING id',
            values: [id, playlist_id, song_id, createAt, createAt]
        }
        const query1 = {
            text: 'SELECT EXISTS(SELECT * FROM playlists WHERE id = $1)',
            values: [playlist_id]
        }
        const result1 = await this._pool.query(query1);
        const playlist = result1.rows[0].exists;
        if (playlist === false) {
            throw new Error('Playlist tidak ditemukan ');
        }
        const query3 = {
            text: 'SELECT EXISTS(SELECT * FROM songs WHERE id = $1)',
            values: [song_id]
        }
        const result3 = await this._pool.query(query3);
        const song = result3.rows[0].exists;
        if (song === false) {
            throw new Error('Lagu tidak ada dalam daftar ');
        }
        const result = await this._pool.query(query);
        if (!result.rows[0].id) {
            throw new Error("Lagu gagal ditambahkan ke playlist");
        }
        return result.rows[0].id;
    }

    async getAllSongPlaylist(id) {
        const query1 = {
            text: 'SELECT EXISTS(SELECT * FROM playlists WHERE id = $1)',
            values: [id]
        }
        const result1 = await this._pool.query(query1);
        const playlist = result1.rows[0].exists;
        if (playlist === false) {
            throw new Error('Playlist tidak ditemukan ');
        }
        const query_playlist = {
            text: 'SELECT EXISTS(SELECT * FROM playlist_songs WHERE playlist_id = $1)',
            values: [id]
        }
        const result2 = await this._pool.query(query_playlist);
        const playlist2 = result2.rows[0].exists;
        if (playlist2 === false) {
            throw new Error('Tidak ada lagu di dalam playlist dengan ID ' + id);
        }

        const query = {
            text: 'SELECT DISTINCT songs.id, songs.title, songs.year, songs.artist, songs.gendre, ' +
                ' songs.duration FROM playlist_songs, playlists, songs ' +
                'WHERE playlist_songs.playlist_id = $1 ',
            values: [id]
        }
        const result = await this._pool.query(query);
        return result.rows;
    }
}

module.exports = PlaylistsService;