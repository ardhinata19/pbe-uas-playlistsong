const {nanoid} = require("nanoid");
const {Pool} = require('pg');

class SongsService {
    constructor() {
        this._pool = new Pool();
    }

    async addSong({title, year, artist, gendre, duration}) {
        const id = 'Song-' + nanoid(10);
        const createAt = new Date().toISOString();

        const query = {
            text: 'INSERT INTO songs VALUES($1,$2,$3,$4,$5,$6,$7,$8) RETURNING id',
            values: [id, title, year, artist, gendre, duration, createAt, createAt]
        }

        const result = await this._pool.query(query);
        if (!result.rows[0].id) {
            throw new Error("Lagu gagal ditambahkan");
        }
        return result.rows[0].id;
    }

    async getSongs() {
        const result = await this._pool.query('SELECT * FROM songs');
        return result.rows;
    }

    async getSongById(id) {
        const query = {
            text: 'SELECT * FROM songs where id=$1',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error('Lagu tidak ditemukan');
        }
        return result.rows[0];
    }

    async editSongById(id, {title, year, artist, gendre, duration}) {
        const updatedAt = new Date().toISOString();

        const query = {
            text: 'UPDATE songs SET title=$1,year=$2,artist=$3,gendre=$4,duration=$5,updated_at=$6 WHERE id=$7 RETURNING id',
            values: [title, year, artist, gendre, duration, updatedAt, id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error('Gagal memperbaharui Lagu, lagu tidak ditemukan');
        }
    }

    async deleteSongById(id) {
        const query_song = {
            text: 'SELECT EXISTS(SELECT * FROM playlist_songs WHERE song_id = $1)',
            values: [id]
        }
        const result1 = await this._pool.query(query_song);
        const song = result1.rows[0].exists;

        if (song === true) {
            throw new Error('Lagu tidak dapat dihapus, Lagu ada di playlist');
        }
        const query = {
            text: 'DELETE FROM songs WHERE id=$1 RETURNING id',
            values: [id]
        }
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new Error('Gagal menghapus Lagu, Lagu tidak ditemukan');
        }
    }

}

module.exports = SongsService;