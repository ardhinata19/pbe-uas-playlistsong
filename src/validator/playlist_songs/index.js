const PlaylistSongPayloadSchema = require("./schema");
const PlaylistSongsValidator = {
    validddatePlaylistSongPayload: (payload) => {
        const validationResult = PlaylistSongPayloadSchema.validate(payload);
        if (validationResult.error) {
            throw new Error(validationResult.error.message);
        }
    }
}
module.exports = PlaylistSongsValidator;