const Joi = require('joi');

const PlaylistSongPayloadSchema = Joi.object({

    song_id: Joi.string().required().messages({
        'string.base': 'song_id harus string',
        'string.empty': 'song_id tidak boleh kosong',
        'any.required': 'song_id dibutuhkan',
    }),
});
module.exports = PlaylistSongPayloadSchema;