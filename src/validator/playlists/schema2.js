const Joi = require('joi');

const PlaylistPayloadSchema = Joi.object({
    song_id: Joi.string().required()
});

module.exports = PlaylistPayloadSchema;
